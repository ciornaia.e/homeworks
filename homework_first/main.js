/*

  Задание 1.

  Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
  Каждая перезагрузка страницы будет с новым цветом.
  Для написания используйте функцию на получение случайного целого числа,
  между минимальным и максимальным значением (Приложена снизу задания)

  + Бонус, повесить обработчик на кнопку через метод onClick
  + Бонус, использовать 16-ричную систему исчесления и цвет HEX -> #FFCC00
  + Бонус выводить полученый цвет по центру страницы.

  Необходимо создать блок через createElement задать ему стили через element.style
  и вывести через appendChild или insertBefore

  Необходимые материалы:
    Math.Random (Доки): https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    __
    Работа с цветом:
    Вариант 1.
      Исользовать element.style.background = 'rgb(r,g,b)';
      где r,g,b случайное число от 0 до 255;

    Вариант 2.
      Исользовать element.style.background = '#RRGGBB';
      где, RR, GG, BB, значние цвета в 16-ричной системе исчесления
      Формирование цвета в вебе: https://ru.wikipedia.org/wiki/%D0%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML
      Перевод в 16-ричную систему исчесления делается при помощи
      метода Number.toString( 16 ) https://www.w3schools.com/jsref/jsref_tostring_number.asp,

      var myNumber = '251'
      myNumber.toString(16) // fb

*/

    var r, g, b, bgColorRGB, bgColorHEX,
        app = document.getElementById('app'),
        coloredContainer = document.createElement('div'),
        controlButton = document.createElement('button'),
        colorOutput = document.createElement('p');

        app.style.height = '100vh';
        coloredContainer.style.cssText = 'height: 100vh; display: flex; flex-direction: column; align-items: center; justify-content: center;';
        controlButton.innerText = 'Change background';

        function getRandomIntInclusive(min, max) {
          min = Math.ceil(min);
          max = Math.floor(max);
          return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        function componentToHex(rgb) {
          rgb = getRandomIntInclusive(0, 255);
          component = rgb.toString(16);
          return component.length === 1 ? '0' + component : component;
        }

        function changeBgColor() {
          r = getRandomIntInclusive(0, 255),
          g = getRandomIntInclusive(0, 255),
          b = getRandomIntInclusive(0, 255),
          bgColorRGB = 'rgb('+r+','+g+','+b+')',
          bgColorHEX = "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);

          coloredContainer.style.background = bgColorRGB;
          colorOutput.innerHTML = 'Background color RGB is: ' + bgColorRGB + '<br>' + 'Background color HEX is: ' + bgColorHEX;
        }

        window.onload = function() {
          changeBgColor();
        };

        controlButton.addEventListener('click', function changeBgOnclick(){
          changeBgColor();
        }, false);

        coloredContainer.appendChild(controlButton);
        coloredContainer.appendChild(colorOutput);
        app.appendChild(coloredContainer);
